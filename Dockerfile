FROM cimg/php:8.3.17

RUN sudo apt-get --allow-releaseinfo-change update

RUN sudo apt-get update && sudo apt-get install -y --install-recommends \
    apt-utils \
    libicu-dev \
    libzip-dev \
    libpng-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libmagickwand-dev \
    libfreetype6-dev \
    libxml2-dev \
    librabbitmq-dev \
    procps \
    openssl \
    redis-tools \
    zip \
    unzip \
    iputils-ping \
    msmtp \
    nano

# Install required packages for PHP extensions
RUN sudo docker-php-ext-configure gd --with-freetype --with-jpeg

RUN sudo docker-php-ext-install \
    sysvsem \
    gd \
    zip \
    pdo \
    pcntl \
    mysqli \
    pdo_mysql \
    sockets \
    xml \
    bcmath

# Install imagick
# TODO RUN sudo pecl install imagick && sudo docker-php-ext-enable imagick

# Install amqp extension
RUN sudo pecl install amqp && sudo docker-php-ext-enable amqp

RUN sudo pecl install trader && sudo docker-php-ext-enable trader
RUN sudo echo -e "trader.real_precision=8" | sudo tee /usr/local/etc/php/php.ini
